﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollControllerCategory : ScrollController
{
    public override void ShowView()
    {
        scrollView.gameObject.SetActive(true);

        AddList("ime", GlobalDataDemo.GetListForScroll());
        ShowItemWithAnimation();
    }

    public override void CloseView() 
    {
        scrollView.gameObject.SetActive(false); 
    }
}

