﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollItemControllerDemoA : ScrollItemController
{

    public override void OnClick()
    {
        DataDemo dataWrapper = (DataDemo)data;
        Debug.Log("OnClick : " + dataWrapper.dataID + "." + dataWrapper.dataName);
    }

    public override void RefreshUnlockItemData()
    {
        throw new System.NotImplementedException();
    }

    public override void SetData(object objData)
    {
        data = objData;
    }

    public override void SetView()
    {
        DataDemo dataWrapper = (DataDemo) data;
        ((ScrollItemViewDemoA)scrollItemView).txtName.text = dataWrapper.dataID + "." + dataWrapper.dataName;
    }

    public override void ShowWithAnimation()
    {
        gameObject.GetComponent<Animator>().SetTrigger("playFlag");
    }
}