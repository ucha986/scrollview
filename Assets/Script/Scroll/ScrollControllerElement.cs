﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollControllerElement : ScrollController
{
    public string categoryName;
    public override void ShowView()
    {
        scrollView.gameObject.SetActive(true);


        AddList("ime", GlobalDataDemo.GetListElementForScroll(categoryName));
        ShowItemWithAnimation();
    }

    public override void CloseView() 
    {
        scrollView.gameObject.SetActive(false);
    }
}

