﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScrollView : MonoBehaviour    {

    protected Transform contentScroll;
	protected GameObject buttonEmpty;
	public ScrollController sccrollController;
    public bool isVertical = false;

	public void Awake()
	{
        contentScroll = transform.Find("Viewport").Find("Content");
        buttonEmpty = contentScroll.Find("ButtonEmpty").gameObject;
		buttonEmpty.SetActive (false);

		transform.GetComponent<ScrollRect>().onValueChanged.AddListener(delegate {  OnScrolling(); });

        isVertical = GetComponent<ScrollRect>().vertical;
	}

	protected bool onClear = false;

	void ClearFlag()
	{
//		Debug.Log("ClearFlag");
		onClear = false;

		//treba nam da bismo prvi put pokrenuli ciscenje scrolla
		OnScrolling();
	}

	public GameObject AddButton()
	{
		GameObject newButton = Instantiate (buttonEmpty, buttonEmpty.transform.position, buttonEmpty.transform.rotation) as GameObject;
		newButton.SetActive (true);
        newButton.transform.SetParent(contentScroll);
		newButton.transform.localScale = buttonEmpty.transform.localScale;

		return newButton;
	}

	public ScrollItemController AddButton(string componentName, object data)
	{
		GameObject newButton = AddButton ();

		//newButton.AddComponent(System.Type.GetType(componentName));
		//newButton.GetComponent (System.Type.GetType(componentName)).SendMessage ("SetData", data);

		return newButton.GetComponent<ScrollItemController>();
	}

	bool inScrollFunction = false;
	public void OnScrolling()
	{
		isCalled = true;
	}

	bool isCalled = false;

	string tempNameScrollTo = "";
	public void ScrollToLast()										//na play dugme scroll postavimo na zadnji chapter
	{
        tempNameScrollTo = "ButtonEmpty" + "_5";
        tempIndexScrollTo = -1;
    }

    int tempIndexScrollTo = -1;
    public void ScrollToIndex(int index)
    {
        tempNameScrollTo = "null";
        tempIndexScrollTo = index;

        ScrollToPosition();
    }

	public void ScrollToPosition()			//scrolamo do zadatog  chaptera
	{
//		Debug.Log("ScrollToPosition in " +transform.parent.name+ " : " + tempNameScrollTo);
		int index = -1;
		int.TryParse(tempNameScrollTo, out index);

		if(index > -1)
		{
			
            float spaceInScroll = isVertical ? contentScroll.GetComponent<VerticalLayoutGroup>().spacing : contentScroll.GetComponent<HorizontalLayoutGroup>().spacing;
			float leftTop = isVertical ? contentScroll.GetComponent<VerticalLayoutGroup>().padding.top : contentScroll.GetComponent<HorizontalLayoutGroup>().padding.left;
            //float scrollPosition = leftTop;
            float scrollPosition = 0.0f;
            float scrollItemSizeHalf = 0.0f;

            int i = 0;
            foreach(Transform item in contentScroll)
			{
                if(item.name != tempNameScrollTo && i < tempIndexScrollTo)
				{
					float scrollItemSize = isVertical ? item.GetComponent<RectTransform>().rect.height : item.GetComponent<RectTransform>().rect.width;
					scrollPosition += (scrollItemSize + spaceInScroll);
            
					scrollItemSizeHalf = scrollItemSize/2;

				}
				else
				{
					break;
				}
                /*else if(item.name != "ButtonEmpty")
				{
					float scrollItemSize = item.GetComponent<RectTransform>().rect.height;
					if(scrollItemSize > 0)
					{
						scrollPositionY += - (scrollItemSize + spaceInScroll);
					}
				}//*/
                i++;
			}

            float scrollTo = scrollPosition;// + spaceInScroll - scrollItemSizeHalf/2;

            //float max = isVertical ? contentScroll.GetComponent<RectTransform>().rect.height : contentScroll.GetComponent<RectTransform>().rect.width;
            //max 
            //scrollTo = scrollTo >
//			Debug.Log("ScrollToPositionY : " + scrollToY);
            if(isVertical)
                contentScroll.GetComponent<RectTransform>().anchoredPosition =  new Vector2(contentScroll.GetComponent<RectTransform>().anchoredPosition.x, scrollTo);
            else
                contentScroll.GetComponent<RectTransform>().anchoredPosition = new Vector2(-scrollTo, contentScroll.GetComponent<RectTransform>().anchoredPosition.y);

        }
    }
}