﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ScrollItemView : MonoBehaviour
{
    protected Button button;
    public ScrollItemController itemController;

    public void Start()
    {
        button = gameObject.GetComponent<Button>();
        ButtonSetup(button);


        if (gameObject.name != "ButtonEmpty")
        {
            EventManager.destroyScrollItem += DestroyMySelf;
        }
    }

    protected void ButtonSetup(Button but)
    {
        but.onClick.RemoveAllListeners();
        but.onClick.AddListener(handleButton);
    }

    void handleButton()
    {
        //      Debug.Log("Button pressed!");
        OnClick();
        //TODO: SoundManager.PlayClick();
        //WebelinxCMS.Instance.ShowAd("Click");
    }

    public void DestroyMySelf()
    {
        if (gameObject.name != "ButtonEmpty")
        {
            EventManager.destroyScrollItem -= DestroyMySelf;
            Debug.Log("DestroyItem");
            Destroy(gameObject);
        }
    }

    public abstract void OnLanguageChange();
    public abstract void OnClick();

    //ovo nam sluzi da namestimo On akciju sa odredjenim indeksom
    public void setOnOnClickAction(int index)
    {
        button.onClick.SetPersistentListenerState(index, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
    }

    //ovo nam sluzi da namestimo Off akciju sa odredjenim indeksom
    public void setOffOnClickAction(int index)
    {
        button.onClick.SetPersistentListenerState(index, UnityEngine.Events.UnityEventCallState.Off);
    }
}
