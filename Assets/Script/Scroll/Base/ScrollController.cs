﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScrollController : MonoBehaviour
{
    List<object> listOfData;
    protected List<ScrollItemController> listScrollItems;


    public ScrollView scrollView;

    void RefreshPrice()
    {
        foreach (ScrollItemController item in listScrollItems)
        {
            if (item.name != "ButtonEmpty")
            {
                item.RefreshUnlockItemData();
            }
        }
    }


    public void ClearScroll()
    {
        if (listOfData == null)
            listOfData = new List<object>();

        if (listScrollItems == null)
            listScrollItems = new List<ScrollItemController>();

        //Debug.Log("----------------------------------------------------ClearScroll");
        EventManager.DestroScrollItems();
        listOfData.Clear();
        listScrollItems.Clear();
    }


    public void AddList(string name, List<object> listItems)
    {
        //onClear = true;
        ClearScroll();

        foreach (object obj in listItems)
        {
            listOfData.Add(obj);
            ScrollItemController scrollItemController = scrollView.AddButton(name, obj);
            scrollItemController.SetData(obj);
            scrollItemController.SetView();

            listScrollItems.Add(scrollItemController);
        }
        scrollView.Invoke("ClearFlag", 2f);
    }

    public abstract void ShowView();
    public abstract void CloseView();
    //{
    //    AddList("ime", GlobalDataDemo.GetListForScroll());

    //    ShowItemWithAnimation();
    //}

    public int GetCountOfItem()
    {
        if (listOfData == null) return 0;

        return listOfData.Count;
    }

    public void ScrollToIndex(int index)
    {
        scrollView.ScrollToIndex(index);
    }



    public void ShowItemWithAnimation()
    {
        StopCoroutine("ShowItemWithAnimationCorutine");
        StartCoroutine("ShowItemWithAnimationCorutine");
    }

    IEnumerator ShowItemWithAnimationCorutine()
    {
        foreach (ScrollItemController controller in listScrollItems)
        {
            controller.ShowWithAnimation();
            yield return new WaitForSeconds(0.1f);
        }
    }
}