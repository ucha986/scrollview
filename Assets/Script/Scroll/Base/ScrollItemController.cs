﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Scene: MainMenu
/// Object: abstract klasa, ne kaci se direktno za objekat
/// Description:    Roditeljska klasa za sve iteme u bilo kom scroll-u
///                 Ova klasa opisuje Item preko podataka (T data) i akcija OnClick
/// </summary>
public abstract class ScrollItemController : MonoBehaviour
{
    //podatak koji opisuje Item trebalo bi da nasledjuje Object
    public object data;
    public ScrollItemView scrollItemView;

    //postavljamo parametre item-a
    public abstract void SetData(object objData);
    public abstract void SetView();

    public abstract void OnClick();

    public abstract void RefreshUnlockItemData();
    public abstract void ShowWithAnimation();
}