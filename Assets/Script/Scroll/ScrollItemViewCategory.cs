﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollItemViewCategory : ScrollItemView
{
    public Text txtName;

    private void Awake()
    {
        txtName = transform.Find("Text").GetComponent<Text>();
    }

    public override void OnClick()
    {
        GetComponent<ScrollItemControllerCategory>().OnClick();
    }

    public override void OnLanguageChange()
    { 

    }
}
