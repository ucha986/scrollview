﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollItemControllerCategory : ScrollItemController
{
    public override void OnClick()
    {
        DataCategory dataCategory = (DataCategory)data;
        Debug.Log("OnClick : ScrollItemControllerCategory : " + dataCategory.categoryID + "." + dataCategory.categoryName);

        GameObject.Find("ScriptHolder").GetComponent<TestDemo>().ChooseCategory(dataCategory.categoryName);
    }

    public override void RefreshUnlockItemData()
    {
        throw new System.NotImplementedException();
    }

    public override void SetData(object objData)
    {
        data = objData;
    }

    public override void SetView()
    {
        DataCategory dataWrapper = (DataCategory)data;
        ((ScrollItemViewCategory)scrollItemView).txtName.text = dataWrapper.categoryID + "." + dataWrapper.categoryName;
    }

    public override void ShowWithAnimation()
    {
        gameObject.GetComponent<Animator>().SetTrigger("playFlag");
    }
}

