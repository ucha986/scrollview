﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static System.Action destroyScrollItem;

    public static void DestroScrollItems()
    {
        if (destroyScrollItem != null)
            destroyScrollItem();
    }
}
