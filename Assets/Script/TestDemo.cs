﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestDemo : MonoBehaviour
{
    public ScrollControllerCategory scrollControllerCategory;
    public ScrollControllerElement scrollControllerElement;

    public Text txtIndex;

    string tempCategory = "";

    void Awake()
    {
        GlobalDataDemo.ReadData();

        //scrollControllerCategory = gameObject.GetComponent<ScrollControllerCategory>();
        //scrollControllerElement = gameObject.GetComponent<ScrollControllerElement>();
    }

    private void Start()
    {
        scrollControllerCategory.ShowView();
        scrollControllerElement.CloseView();
    }

    int index = 0;
    public void OnClikPrev()
    {
        index = (index - 1 < 0) ? 0 : (index - 1);
        scrollControllerCategory.ScrollToIndex(index);
        txtIndex.text = index.ToString();
    }


    public void OnClikNext()
    {
        index = (index + 1 > scrollControllerCategory.GetCountOfItem() - 1) ? scrollControllerCategory.GetCountOfItem() -1 : (index + 1);
        scrollControllerCategory.ScrollToIndex(index);
        txtIndex.text = index.ToString();
    }

    public void ChooseCategory(string catName)
    {
        tempCategory = catName;
        scrollControllerCategory.CloseView();
        scrollControllerElement.categoryName = catName;
        scrollControllerElement.ShowView(); 
    }

    public void OnClickClose()
    {
        if(tempCategory != "")
        {
            tempCategory = "";
            scrollControllerElement.CloseView();
            scrollControllerCategory.ShowView();
        }
    }
}
