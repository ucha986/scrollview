﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DataAll
{
    public List<DataCategory> listCategory;

    public List<DataDemo> GetDataForCategory(string catName)
    {
        DataCategory dataC = listCategory.Where(c => c.categoryName == catName).FirstOrDefault();
        if(dataC != null)
        {
            return dataC.listDataElements; 
        }

        return null;
    }
}

public class DataCategory
{
    public string categoryID;
    public string categoryName;

    public List<DataDemo> listDataElements = new List<DataDemo>();
}

public class DataDemo
{
    public string dataID;
    public string dataName;
}

public static class GlobalDataDemo
{
    public static DataAll dataAll;

    public static void ReadData()
    {
        List<DataCategory> listDataCategory = new List<DataCategory>();
        DataCategory dataC = new DataCategory { categoryName = "sto", categoryID = "0" };

        List<DataDemo> listDataDemos = new List<DataDemo>();
        listDataDemos.Add(new DataDemo() { dataID = "11", dataName = "Nemanja" });
        listDataDemos.Add(new DataDemo() { dataID = "12", dataName = "Stud" });
        listDataDemos.Add(new DataDemo() { dataID = "13", dataName = "Trew" });
        listDataDemos.Add(new DataDemo() { dataID = "14", dataName = "Imam" });
        listDataDemos.Add(new DataDemo() { dataID = "15", dataName = "Sttbrt" });
        listDataDemos.Add(new DataDemo() { dataID = "16", dataName = "KaoKako" });
        listDataDemos.Add(new DataDemo() { dataID = "17", dataName = "End" });

        dataC.listDataElements = listDataDemos;
        listDataCategory.Add(dataC);

        listDataDemos.Clear();
        dataC = new DataCategory { categoryName = "stolice", categoryID = "1" };
        listDataDemos.Add(new DataDemo() { dataID = "21", dataName = "Nemanja" });
        listDataDemos.Add(new DataDemo() { dataID = "22", dataName = "Stud" });
        listDataDemos.Add(new DataDemo() { dataID = "23", dataName = "Trew" });
        listDataDemos.Add(new DataDemo() { dataID = "24", dataName = "Imam" });
        listDataDemos.Add(new DataDemo() { dataID = "25", dataName = "Sttbrt" });
        listDataDemos.Add(new DataDemo() { dataID = "26", dataName = "KaoKako" });
        listDataDemos.Add(new DataDemo() { dataID = "27", dataName = "End" });

        dataC.listDataElements = listDataDemos;
        listDataCategory.Add(dataC);

        listDataDemos.Clear();
        dataC = new DataCategory { categoryName = "ofingeri", categoryID = "2" };
        listDataDemos.Add(new DataDemo() { dataID = "31", dataName = "Nemanja" });
        listDataDemos.Add(new DataDemo() { dataID = "32", dataName = "Stud" });
        listDataDemos.Add(new DataDemo() { dataID = "33", dataName = "Trew" });
        listDataDemos.Add(new DataDemo() { dataID = "34", dataName = "Imam" });
        listDataDemos.Add(new DataDemo() { dataID = "35", dataName = "Sttbrt" });
        listDataDemos.Add(new DataDemo() { dataID = "36", dataName = "KaoKako" });
        listDataDemos.Add(new DataDemo() { dataID = "37", dataName = "End" });

        dataC.listDataElements = listDataDemos;
        listDataCategory.Add(dataC);

        listDataDemos.Clear();
        dataC = new DataCategory { categoryName = "peskiri", categoryID = "1" };
        listDataDemos.Add(new DataDemo() { dataID = "41", dataName = "Nemanja" });
        listDataDemos.Add(new DataDemo() { dataID = "42", dataName = "Stud" });
        listDataDemos.Add(new DataDemo() { dataID = "43", dataName = "Trew" });
        listDataDemos.Add(new DataDemo() { dataID = "44", dataName = "Imam" });
        listDataDemos.Add(new DataDemo() { dataID = "45", dataName = "Sttbrt" });
        listDataDemos.Add(new DataDemo() { dataID = "46", dataName = "KaoKako" });
        listDataDemos.Add(new DataDemo() { dataID = "47", dataName = "End" });

        dataC.listDataElements = listDataDemos;
        listDataCategory.Add(dataC);

        dataAll = new DataAll();
        dataAll.listCategory = listDataCategory;
    }

    public static List<object> GetListForScroll()
    {
        List<object> listTemp = new List<object>();

        foreach (DataCategory data in dataAll.listCategory)
            listTemp.Add((object)data);

        return listTemp;
    }

    public static List<object> GetListElementForScroll(string catName)
    {
        List<object> listTemp = new List<object>();

        List<DataDemo> listElement = dataAll.GetDataForCategory(catName);
        if (listElement != null)
        {
            foreach (DataDemo data in listElement)
                listTemp.Add((object)data);
        }

        return listTemp;
    }
}